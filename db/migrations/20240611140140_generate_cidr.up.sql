ALTER TABLE geo
    ADD COLUMN hosts inet GENERATED ALWAYS AS (inet_merge(ip_range_start, ip_range_end)) STORED
    