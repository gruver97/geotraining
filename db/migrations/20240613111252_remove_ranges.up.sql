alter table geo
    DROP column hosts;

ALTER TABLE geo
    DROP COLUMN ip_range_start,
    DROP COLUMN ip_range_end