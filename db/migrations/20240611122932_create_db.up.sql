CREATE TABLE geo
(
    ip_range_start  inet,
    ip_range_end    inet,
    as_number       INT,
    as_organization TEXT,
    country_code    varchar(2),
    state1          TEXT,
    state2          TEXT,
    city            TEXT,
    postcode        TEXT,
    latitude        point,
    longitude       point,
    timezone        int,
    -- country
    Name            TEXT,
    Native          TEXT,
    Phone           TEXT,
    Continent       TEXT,
    Capital         TEXT,
    Currency        TEXT,
    Languages       text[]
)