package address

import (
	"context"
	"fmt"
	"net"

	"github.com/projectdiscovery/mapcidr"
)

func GetIPFromRange(ctx context.Context, first, last net.IP) (<-chan net.IP, error) {
	ipChan := make(chan net.IP, 1_000_000)

	cidrs, err := mapcidr.GetCIDRFromIPRange(first, last)
	if err != nil {
		return ipChan, fmt.Errorf("cannot get CIDR from IP range %v - %v: %w", first, last, err)
	}

	go func() {
		defer close(ipChan)

		for _, cidr := range cidrs {
			select {
			case <-ctx.Done():
				return
			default:
				for ipStr := range mapcidr.IpAddresses(cidr) {
					ipChan <- net.ParseIP(ipStr)
				}
			}
		}
	}()

	return ipChan, nil
}
