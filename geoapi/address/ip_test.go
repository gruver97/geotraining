package address

import (
	"context"
	"net"
	"testing"

	"github.com/projectdiscovery/mapcidr"
	"github.com/stretchr/testify/require"
)

func TestGetIPFromRange(t *testing.T) {
	t.Parallel()

	ipStart, ipEnd := net.ParseIP("1.0.0.0"), net.ParseIP("1.0.0.255")
	ipChan, err := GetIPFromRange(context.Background(), ipStart, ipEnd)
	require.NoError(t, err)

	actualIpCount := 0
	for ip := range ipChan {
		if ip == nil {
			t.Fail()
		}
		actualIpCount++
	}

	ipRange, err := mapcidr.GetCIDRFromIPRange(ipStart, ipEnd)
	require.NoError(t, err)
	expectedCount := mapcidr.CountIPsInCIDRs(true, true, ipRange...)

	require.EqualValues(t, expectedCount.Uint64(), uint64(actualIpCount))
}
