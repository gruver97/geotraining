package main

import (
	"context"
	"geoapi/address"
	"geoapi/geo/countries"
	"geoapi/geo/filesystem"
	"geoapi/storage"
	"geoapi/storage/files"
	"geoapi/storage/postgres"
	"log/slog"
	"os"
	"sync"
)

var Version string

func main() {
	slog.Info("Starting initor", "version", Version)
	defer slog.Info("Stopping initor")

	ctx := context.Background()
	store, err := postgres.New(ctx, os.Getenv("DATABASE_URL"))
	if err != nil {
		slog.Error("Unable to connect to database", "error", err)

		return
	}
	defer store.Close()

	err = store.Ping(ctx)
	if err != nil {
		slog.Error("Error pinging database", "error", err)

		return
	}
	slog.Info("ping ok")

	countriesProvider := filesystem.NewProvider("/bin/geoapi/ip_countries.csv")

	list := countries.New(countriesProvider)
	ch, _ := list.GetAll(ctx)
	//ch, _ := list.GetAllByFirstDigit(ctx, 1)

	slog.Info("start handling ip")
	ipCountryCh := make(chan storage.IPGeoInfo, 100_000)

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()

		err := files.Write("/bin/geoapi/ip_countries", ipCountryCh)
		if err != nil {
			slog.Error("Error writing ip", "error", err)

			panic(err)
		}
	}()

	for c := range ch {
		slog.Info("start handling country", slog.String("country", c.CountryCode), slog.String("start", c.IPStart.String()), slog.String("end", c.IPEnd.String()))
		ipChan, err := address.GetIPFromRange(ctx, c.IPStart, c.IPEnd)
		if err != nil {
			slog.Error("Error getting IP", "start", c.IPStart, "end", c.IPEnd, "error", err)
			return
		}

		for ip := range ipChan {
			ipCountryCh <- storage.IPGeoInfo{IP: ip, CountryCode: c.CountryCode}
		}
	}

	close(ipCountryCh)

	wg.Wait()

	err = store.CopyIPFromStream(ctx, files.Read("/bin/geoapi/ip_countries"))
	if err != nil {
		slog.Error("Error write database", "error", err)

		return
	}

	//for i := 1; i <= 255; i++ {
	//	fmt.Printf("handle %d\n", i)
	//	cl, errCh := list.GetAllByFirstDigit(ctx, i)
	//	ipCountryCh := make(chan storage.IPGeoInfo, 17_000_000)
	//	memoryStorage := memory.New(ipCountryCh)
	//
	//	wg := sync.WaitGroup{}
	//
	//	// read country file
	//	wg.Add(1)
	//	go func() {
	//		slog.Info("start handling ip")
	//		defer slog.Info("finish handling ip")
	//		defer wg.Done()
	//		defer close(ipCountryCh)
	//
	//		for {
	//			select {
	//			case err := <-errCh:
	//				if err != nil {
	//					slog.Error("Error fetching country", "error", err)
	//					return
	//				}
	//			case country, ok := <-cl:
	//				if !ok {
	//					return
	//				}
	//
	//				ipChan, err := address.GetIPFromRange(ctx, country.IPStart, country.IPEnd)
	//				if err != nil {
	//					slog.Error("Error getting IP", "start", country.IPStart, "end", country.IPEnd, "error", err)
	//					return
	//				}
	//
	//				for ip := range ipChan {
	//					ipCountryCh <- storage.IPGeoInfo{IP: ip, CountryCode: country.CountryCode}
	//				}
	//			}
	//		}
	//	}()
	//
	//	// merge ips
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//
	//		memoryStorage.ListenAndMerge(ctx)
	//	}()
	//
	//	wg.Wait()
	//
	//	slog.Info("start write to db")
	//	ips := memoryStorage.GetAll(ctx)
	//
	//	wg.Add(1)
	//
	//	go func() {
	//		defer wg.Done()
	//
	//		err = store.WriteIPFromStream(ctx, ips)
	//		if err != nil {
	//			slog.Error("error insert ip to db", "error", err)
	//
	//			return
	//		}
	//	}()
	//
	//	wg.Wait()
	//}
}
