package main

import (
	"context"
	"fmt"
	"geoapi/address"
	"geoapi/geo/countries"
	"geoapi/geo/filesystem"
	"geoapi/storage"
	"geoapi/storage/files"
	"geoapi/storage/postgres"
	_ "go.uber.org/automaxprocs"
	"log/slog"
	"os"
	"sync"
)

var Version string

func main() {
	slog.Info("Starting renewer", "version", Version)
	defer slog.Info("Stopping renewer")

	const workers = 4

	ips := make(chan int, 255)
	for i := 1; i <= 255; i++ {
		ips <- i
	}

	wg := &sync.WaitGroup{}
	wg.Add(workers)
	for i := 0; i < workers; i++ {
		go run(wg, ips)
	}
	wg.Wait()
}

func run(wg *sync.WaitGroup, ips chan int) {
	defer wg.Done()

	ctx := context.Background()
	store, err := postgres.New(ctx, os.Getenv("DATABASE_URL"))
	if err != nil {
		slog.Error("Unable to connect to database", "error", err)

		return
	}
	defer store.Close()

	err = store.Ping(ctx)
	if err != nil {
		slog.Error("Error pinging database", "error", err)

		return
	}
	slog.Info("ping ok")

	countriesProvider := filesystem.NewProvider("/bin/geoapi/ip_countries.csv" /*"/Users/stanislavgerasko/Projects/geotraining/geoapi/ip_countries.csv"*/)
	list := countries.New(countriesProvider)

	for ip := range ips {
		fileName := fmt.Sprintf("/bin/geoapi/ip_countries_%d", ip)
		//fileName := fmt.Sprintf("ip_countries_%d", ip)

		slog.Info("start writing file", slog.String("fileName", fileName))

		wg2 := &sync.WaitGroup{}
		wg2.Add(1)

		ipCountryCh := make(chan storage.IPGeoInfo, 1_000_000)

		go func() {
			defer wg2.Done()

			slog.Info("start handling ip")

			defer close(ipCountryCh)

			ch, _ := list.GetAllByFirstDigit(ctx, ip)

			for c := range ch {
				//slog.Info("start handling country", slog.String("country", c.CountryCode), slog.String("start", c.IPStart.String()), slog.String("end", c.IPEnd.String()))
				ipChan, err := address.GetIPFromRange(ctx, c.IPStart, c.IPEnd)
				if err != nil {
					slog.Error("Error getting IP", "start", c.IPStart, "end", c.IPEnd, "error", err)
					return
				}

				for ip := range ipChan {
					ipCountryCh <- storage.IPGeoInfo{IP: ip, CountryCode: c.CountryCode}
				}
			}
		}()

		err := files.Write(fileName, ipCountryCh)
		if err != nil {
			slog.Error("Error writing ip", "error", err)

			panic(err)
		}

		slog.Info("end writing file", slog.String("fileName", fileName))

		wg2.Wait()

		err = store.UpdateWithPartition(ctx, files.Read(fileName), ip, ip)
		if err != nil {
			slog.Error("Error write database", "error", err)

			panic(err)
		}
	}
}
