package main

import (
	"context"
	"fmt"
	"log"
	"log/slog"
	"net"
	"os"

	"geoapi"
	pb "geoapi/server"
	"geoapi/storage/postgres"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var Version string

func main() {
	slog.Info("Starting api", "version", Version)
	defer slog.Info("Stopping api")

	ctx := context.Background()

	storage, err := postgres.New(ctx, os.Getenv("DATABASE_URL"))
	if err != nil {
		slog.Error("Unable to connect to database", "error", err)

		return
	}
	defer storage.Close()

	err = storage.Ping(ctx)
	if err != nil {
		slog.Error("Error pinging database", "error", err)

		return
	}
	slog.Info("ping ok")

	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", 50051))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	srv := geoapi.NewServer(nil)
	pb.RegisterGeoInfoServer(grpcServer, srv)
	reflection.Register(grpcServer)
	grpcServer.Serve(lis)
	defer grpcServer.GracefulStop()
}
