package postgres

import (
	"context"
	"fmt"
	"geoapi/storage"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"log/slog"
	"strconv"
)

type Storage struct {
	DB *pgxpool.Pool
}

func New(ctx context.Context, connString string) (*Storage, error) {
	config, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return nil, err
	}

	config.ConnConfig.DefaultQueryExecMode = pgx.QueryExecModeDescribeExec

	if pool, err := pgxpool.NewWithConfig(ctx, config); err != nil {
		return nil, fmt.Errorf("failed to connect to database: %w", err)
	} else {
		return &Storage{DB: pool}, nil
	}
}

func (s *Storage) Close() {
	s.DB.Close()
}

func (s *Storage) Ping(ctx context.Context) error {
	return s.DB.Ping(ctx)
}

func (s *Storage) WriteIPFromStream(ctx context.Context, ip <-chan storage.IPGeoInfo) error {
	sql := `INSERT INTO geo AS g (ip_host, country_code) VALUES ($1, $2)
ON CONFLICT (ip_host) DO UPDATE
    SET country_code = excluded.country_code;`

	var currentBatch pgx.Batch

	for geoIP := range ip {
		if currentBatch.Len() < 1_000_000 {
			currentBatch.Queue(sql, geoIP.IP, geoIP.CountryCode)
		} else {
			res := s.DB.SendBatch(ctx, &currentBatch)
			err := res.Close()
			if err != nil {
				return err
			}

			fmt.Printf("written %d records \n", currentBatch.Len())

			currentBatch = pgx.Batch{}
		}
	}

	res := s.DB.SendBatch(ctx, &currentBatch)
	err := res.Close()
	if err != nil {
		return err
	}

	fmt.Printf("written %d records \n", currentBatch.Len())

	currentBatch = pgx.Batch{}

	return nil
}

func (s *Storage) CopyIPFromStream(ctx context.Context, ip <-chan storage.IPGeoInfo) error {
	sql := `CREATE TEMP TABLE  temp_geo (ip_host inet, country_code varchar(2))`
	_, err := s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}

	c := CopyFromChan(ip)
	res, err := s.DB.CopyFrom(ctx, []string{"temp_geo"}, []string{"ip_host", "country_code"}, c)
	if err != nil {
		return err
	}

	fmt.Printf("written %d records \n", res)

	fmt.Printf("start insert \n")

	sql = `INSERT INTO geo (ip_host, country_code) (SELECT ip_host, country_code FROM temp_geo);`

	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}

	fmt.Printf("done insert \n")

	fmt.Printf("start indexing\n")
	sql = `create index on geo using gist(ip_host inet_ops)`
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	fmt.Printf("stop indexing\n")

	fmt.Printf("start clean ip_host\n")
	sql = `DELETE FROM geo WHERE ip_host IS NULL`
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	fmt.Printf("stop clean ip_host\n")

	fmt.Printf("start creation of primary key\n")
	sql = `ALTER TABLE geo ADD PRIMARY KEY (ip_host);`
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	fmt.Printf("stop creation of primary key\n")

	return nil
}

func (s *Storage) UpdateWithPartition(ctx context.Context, ip <-chan storage.IPGeoInfo, ipDigit int, timestamp int) error {
	oldPartitionName := fmt.Sprintf("range_%d", ipDigit)
	newPartitionName := fmt.Sprintf("range_%d_%d", ipDigit, timestamp)
	tempNewPartitionName := fmt.Sprintf("temp_%s", newPartitionName)

	sql := fmt.Sprintf(`CREATE TABLE %s (LIKE geo INCLUDING DEFAULTS INCLUDING CONSTRAINTS)`, newPartitionName)
	_, err := s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf(`CREATE temp TABLE %s (ip_host inet, country_code varchar(2))`, tempNewPartitionName)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}

	c := CopyFromChan(ip)
	res, err := s.DB.CopyFrom(ctx, []string{tempNewPartitionName}, []string{"ip_host", "country_code"}, c)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf(`DELETE FROM %s WHERE ip_host IS NULL`, tempNewPartitionName)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	slog.Info("written records", slog.Group(strconv.Itoa(ipDigit), slog.Int64("count", res)), slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))

	sql = fmt.Sprintf("insert into %s (ip_host, country_code) select ip_host, country_code from %s", newPartitionName, tempNewPartitionName)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}

	slog.Info("start delete", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))
	sql = fmt.Sprintf(`delete from %s where ip_host = '%d.255.255.255'`, newPartitionName, ipDigit)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	slog.Info("finish delete", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))

	slog.Info("start add constraint", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))
	sql = fmt.Sprintf(`ALTER TABLE %s ADD CONSTRAINT ip255 CHECK ( ip_host >= '%d.0.0.0' AND ip_host <= '%d.255.255.255');`, newPartitionName, ipDigit, ipDigit)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	slog.Info("finish add constraint", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))

	slog.Info("start detach", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))
	sql = fmt.Sprintf("ALTER TABLE geo DETACH PARTITION %s", oldPartitionName)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	slog.Info("finish detach", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))

	slog.Info("start attach", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))
	sql = fmt.Sprintf(`ALTER TABLE geo ATTACH PARTITION %s FOR VALUES FROM ('%d.0.0.0') TO ('%d.255.255.255')`, newPartitionName, ipDigit, ipDigit)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	slog.Info("finish attach", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))

	slog.Info("start drop", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))
	sql = fmt.Sprintf("DROP TABLE %s", oldPartitionName)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	slog.Info("finish drop", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))

	slog.Info("start rename", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))
	sql = fmt.Sprintf(`ALTER TABLE %s RENAME TO %s`, newPartitionName, oldPartitionName)
	_, err = s.DB.Exec(ctx, sql)
	if err != nil {
		return err
	}
	slog.Info("finish rename", slog.Int("ip", ipDigit), slog.Int("file_suffix", timestamp))

	return nil
}

func CopyFromChan(values <-chan storage.IPGeoInfo) pgx.CopyFromSource {
	return &copyFromChan{dch: values}
}

type copyFromChan struct {
	err error
	dch <-chan storage.IPGeoInfo
}

func (cts *copyFromChan) Next() bool {
	_, ok := <-cts.dch

	return ok
}

func (cts *copyFromChan) Values() ([]any, error) {
	value := <-cts.dch

	return []any{value.IP.To4(), value.CountryCode}, nil
}

func (cts *copyFromChan) Err() error {
	return cts.err
}
