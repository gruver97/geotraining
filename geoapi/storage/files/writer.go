package files

import (
	"geoapi/storage"
	"github.com/golang/snappy"
	jsoniter "github.com/json-iterator/go"
	"log/slog"
	"os"
)

func Write(path string, data <-chan storage.IPGeoInfo) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}

	defer file.Close()

	fileWriter := snappy.NewBufferedWriter(file)

	defer fileWriter.Close()

	var json = jsoniter.ConfigCompatibleWithStandardLibrary

	jenc := json.NewEncoder(fileWriter)

	for d := range data {
		err = jenc.Encode(d)
		if err != nil {
			return err
		}
	}

	stat, err := file.Stat()
	if err != nil {
		return err
	}
	slog.Info("file size is", slog.Int64("bytes", stat.Size()), slog.String("filename", path))

	return nil
}

func Read(path string) <-chan storage.IPGeoInfo {
	res := make(chan storage.IPGeoInfo, 1_000_000)

	go func() {
		defer close(res)

		file, err := os.Open(path)
		if err != nil {
			panic(err)
		}

		defer file.Close()

		fileReader := snappy.NewReader(file)

		var json = jsoniter.ConfigCompatibleWithStandardLibrary

		jdec := json.NewDecoder(fileReader)

		var p = &storage.IPGeoInfo{}

		for jdec.More() {
			err := jdec.Decode(p)
			if err != nil {
				panic(err)
			}
			res <- *p
		}

	}()

	return res
}
