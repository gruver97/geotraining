package storage

import "net"

type IPGeoInfo struct {
	IP          net.IP `json:"ip"`
	CountryCode string `json:"country_code"`
}

func NewIPGeoInfoWithCountry(ip net.IP, countryCode string) *IPGeoInfo {
	return &IPGeoInfo{
		IP:          ip,
		CountryCode: countryCode,
	}
}

func (ipg *IPGeoInfo) HasCountryCode() bool {
	return ipg.CountryCode != ""
}
