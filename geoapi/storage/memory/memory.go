package memory

import (
	"context"
	"log/slog"
	"sync"
	"sync/atomic"

	"geoapi/storage"

	"github.com/projectdiscovery/mapcidr"
)

type Memory struct {
	ips       map[uint64]*storage.IPGeoInfo
	mtx       *sync.RWMutex
	incCnt    atomic.Uint64
	countryCh <-chan storage.IPGeoInfo
}

func New(countriesCh <-chan storage.IPGeoInfo) *Memory {
	return &Memory{
		ips:       make(map[uint64]*storage.IPGeoInfo, 17_000_000),
		countryCh: countriesCh,
		mtx:       &sync.RWMutex{},
	}
}

func (ms *Memory) ListenAndMerge(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case ipgi, ok := <-ms.countryCh:
			if !ok {
				slog.Info("total count", "is", len(ms.ips))
				return
			}

			ms.mtx.Lock()
			ipint, _, _ := mapcidr.IPToInteger(ipgi.IP)
			if ipGeoInfo, found := ms.ips[ipint.Uint64()]; !found {
				ms.ips[ipint.Uint64()] = storage.NewIPGeoInfoWithCountry(ipgi.IP, ipgi.CountryCode)
			} else {
				if !ipGeoInfo.HasCountryCode() {
					ms.ips[ipint.Uint64()].CountryCode = ipgi.CountryCode
				}
			}
			ms.mtx.Unlock()
		}
	}
}

func (ms *Memory) GetAll(ctx context.Context) <-chan storage.IPGeoInfo {
	all := make(chan storage.IPGeoInfo, 17_000_000)

	go func() {
		defer close(all)

		for _, value := range ms.ips {
			all <- *value
		}
	}()

	return all
}
