package countries

import (
	"bytes"
	"context"
	"fmt"
	"log/slog"
	"net"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetAll(t *testing.T) {
	t.Parallel()

	c := New(countriesClientMock{})
	countries, err := c.GetAll(context.Background())

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()

		select {
		case ct, ok := <-countries:
			if !ok {
				return
			}

			require.NotEmpty(t, ct)
			slog.Info("actual country", slog.Any("country", ct))
		case cerr := <-err:
			require.NoError(t, cerr)
		}
	}()

	wg.Wait()
}

type countriesClientMock struct {
}

func (cm countriesClientMock) Countries(ctx context.Context) (*bytes.Buffer, error) {
	var b = new(bytes.Buffer)

	for _, t := range tests {
		b.WriteString(fmt.Sprintf("%s,%s,%s", t.CsvCountryLine[0], t.CsvCountryLine[1], t.CsvCountryLine[2]))
	}

	return b, nil
}

var tests = []struct {
	CsvCountryLine []string
	Expected       Country
}{
	{
		CsvCountryLine: []string{"217.182.5.152", "217.182.5.255", "FR"},
		Expected: Country{
			net.ParseIP("217.182.5.152"),
			net.ParseIP("217.182.5.255"),
			"FR",
		},
	},
}

func TestParse(t *testing.T) {
	t.Parallel()

	for _, test := range tests {
		t.Run(fmt.Sprintf("parse %s", test.Expected.CountryCode), func(t *testing.T) {
			t.Parallel()

			actual, err := Parse(test.CsvCountryLine)
			require.NoError(t, err)
			require.Equal(t, test.Expected.CountryCode, actual.CountryCode)
			require.Equal(t, test.Expected.IPStart, actual.IPStart)
			require.Equal(t, test.Expected.IPEnd, actual.IPEnd)
		})
	}
}

func TestIsFirstIPDigit(t *testing.T) {
	t.Parallel()

	tests := []struct {
		IP         string
		FirstDigit int
	}{
		{
			"1.0.0.1", 1,
		},
		{
			"234.1.4.9", 234,
		},
		{
			"192.168.1.1", 192,
		},
	}

	for _, test := range tests {
		t.Run(test.IP, func(t *testing.T) {
			t.Parallel()

			require.True(t, IsFirstIPDigit(test.IP, test.FirstDigit))
		})
	}
}
