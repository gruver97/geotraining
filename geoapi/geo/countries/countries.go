package countries

import (
	"bytes"
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net"
	"strconv"
	"strings"
)

type List struct {
	provider GeoProvider
}

type GeoProvider interface {
	Countries(ctx context.Context) (*bytes.Buffer, error)
}

func New(provider GeoProvider) List {
	return List{
		provider: provider,
	}
}

type Country struct {
	IPStart     net.IP
	IPEnd       net.IP
	CountryCode string
}

func (l List) GetAll(ctx context.Context) (<-chan Country, <-chan error) {
	countries := make(chan Country, 1000)
	errCh := make(chan error, 1)

	go func() {
		defer close(countries)
		defer close(errCh)

		cts, err := l.provider.Countries(ctx)
		if err != nil {
			errCh <- fmt.Errorf("get countries error: %w", err)

			return
		}
		r := csv.NewReader(cts)

		for {
			rec, e := r.Read()
			if e != nil {
				if errors.Is(e, io.EOF) {
					return
				}
				errCh <- fmt.Errorf("csv read error: %w", e)
				return
			}
			if rec == nil {
				return
			}

			c, e := Parse(rec)
			if e != nil {
				errCh <- fmt.Errorf("csv parse error: %w", e)
				continue
			}
			countries <- c
		}
	}()

	return countries, errCh
}

func (l List) GetAllByFirstDigit(ctx context.Context, digit int) (<-chan Country, <-chan error) {
	countries := make(chan Country, 1_000_000)
	errCh := make(chan error, 1)

	go func() {
		defer close(countries)
		defer close(errCh)

		cts, err := l.provider.Countries(ctx)
		if err != nil {
			errCh <- fmt.Errorf("get countries error: %w", err)

			return
		}
		r := csv.NewReader(cts)

		for {
			rec, e := r.Read()
			if e != nil {
				if errors.Is(e, io.EOF) {
					return
				}
				errCh <- fmt.Errorf("csv read error: %w", e)
				return
			}
			if rec == nil {
				return
			}
			if !IsFirstIPDigit(rec[0], digit) {

				continue
			}
			if !IsFirstIPDigitRange(rec[0], rec[1], digit) {
				slog.Warn("skip range", slog.String("start", rec[0]), slog.String("end", rec[1]))

				continue
			}

			c, e := Parse(rec)
			if e != nil {
				errCh <- fmt.Errorf("csv parse error: %w", e)
				continue
			}

			countries <- c
		}
	}()

	return countries, errCh
}

func Parse(src []string) (Country, error) {
	if len(src) != 3 {
		return Country{}, errors.New("invalid country raw length")
	}

	var c Country
	c.IPStart = net.ParseIP(src[0])
	c.IPEnd = net.ParseIP(src[1])
	c.CountryCode = src[2]

	if c.IPStart == nil {
		return Country{}, errors.New("invalid country start ip")
	}

	if c.IPEnd == nil {
		return Country{}, errors.New("invalid country start ip")
	}

	return c, nil
}

func IsFirstIPDigit(ip string, first int) bool {
	octets := strings.Split(ip, ".")
	if len(octets) != 4 {
		return false
	}

	if d, _ := strconv.Atoi(octets[0]); d == first {
		return true
	}

	return false
}

func IsFirstIPDigitRange(ipStart, ipEnd string, first int) bool {
	octets := strings.Split(ipStart, ".")
	octetsEnd := strings.Split(ipEnd, ".")
	if len(octets) != 4 {
		return false
	}
	if len(octetsEnd) != 4 {
		return false
	}

	if d, _ := strconv.Atoi(octets[0]); d == first {
		if d2, _ := strconv.Atoi(octetsEnd[0]); d2 == first {
			return true
		}
	}

	return false
}
