package client

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

const countriesAddr = "https://cdn.jsdelivr.net/npm/@ip-location-db/geolite2-country/geolite2-country-ipv4.csv"

func TestNew(t *testing.T) {
	t.Parallel()
	cl := New(countriesAddr)
	require.NotNil(t, cl)
}

func TestCountries(t *testing.T) {
	t.Parallel()
	t.Run("200 status", func(t *testing.T) {
		t.Parallel()
		data := []byte("happy data")
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Write(data)
		}))
		defer srv.Close()

		cl := New(srv.URL)
		countries, err := cl.Countries(context.Background())
		require.NoError(t, err)
		require.Len(t, countries.Bytes(), len(data))
	})
	t.Run("error status codes", func(t *testing.T) {
		t.Parallel()
		data := []byte("error data")
		for _, statusCode := range []int{http.StatusBadRequest, http.StatusBadGateway, http.StatusInternalServerError, http.StatusGatewayTimeout, http.StatusUnauthorized, http.StatusNotFound} {
			ft := func(stc int) {
				srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(stc)
					w.Write(data)
				}))
				defer srv.Close()

				cl := New(srv.URL)
				countries, err := cl.Countries(context.Background())
				require.Error(t, err)
				require.Len(t, countries.Bytes(), 0)
			}
			ft(statusCode)
		}

	})
}
