package client

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net/http"
)

type Client struct {
	hcl           http.Client
	countriesAddr string
}

func New(countriesAddress string) *Client {
	return &Client{
		http.Client{
			Transport: &http.Transport{

				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		},
		countriesAddress,
	}
}

func (c Client) Countries(ctx context.Context) (*bytes.Buffer, error) {
	var b = new(bytes.Buffer)
	r, err := http.NewRequestWithContext(ctx, http.MethodGet, c.countriesAddr, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot create countries request: %w", err)
	}

	resp, err := c.hcl.Do(r)
	if err != nil {
		return nil, fmt.Errorf("cannot do get countries request: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return b, errors.New(fmt.Sprintf("unexpected status code: %d", resp.StatusCode))
	}

	_, err = io.Copy(b, resp.Body)
	if err != nil {
		return nil, fmt.Errorf("cannot copy countries response: %w", err)
	}
	return b, nil
}
