package filesystem

import (
	"bytes"
	"context"
	"fmt"
	fileutil "github.com/projectdiscovery/utils/file"
	"os"
)

type Provider struct {
	path string
}

func NewProvider(path string) Provider {
	return Provider{path}
}

func (p Provider) Countries(ctx context.Context) (*bytes.Buffer, error) {
	f, err := p.readFile(ctx, p.path)
	if err != nil {
		return nil, fmt.Errorf("file %s does not exist: %w", p.path, err)
	}

	var b = new(bytes.Buffer)

	_, err = b.Write(f)

	return b, err
}

func (p Provider) readFile(ctx context.Context, path string) ([]byte, error) {
	if !fileutil.FileExists(path) {
		return nil, os.ErrNotExist
	}

	return os.ReadFile(path)
}
