package geoapi

import (
	"context"
	"time"

	"geoapi/geo/countries"
	pb "geoapi/server"

	"google.golang.org/protobuf/types/known/timestamppb"
)

type Server struct {
	pb.UnimplementedGeoInfoServer
	storage GeoInformer
}

type GeoInformer interface {
	GetInfoByIP(ctx context.Context, ip string) countries.Country
}

func NewServer(storage GeoInformer) *Server {
	return &Server{
		storage: storage,
	}
}

func (s Server) GetGeoByIP(ctx context.Context, in *pb.GeoByIPRequest) (*pb.GeoIP, error) {
	return &pb.GeoIP{
		Ip:          in.GetIp(),
		Longitude:   0,
		Latitude:    0,
		CountryCode: "RU",
		LastUpdated: timestamppb.New(time.Now()),
	}, nil
}
