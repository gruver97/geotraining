# geotraining

source https://github.com/sapics/ip-location-db?tab=readme-ov-file

## server

example request `grpcurl -d '{"ip":"1.1.1.1"}' -plaintext localhost:50051 geoapi.GeoInfo/GetGeoByIP`

generate server and client `protoc -IPATH=geoapi --go_out=/Users/stanislavgerasko/Projects/geotraining/geoapi/server --go-grpc_out=/Users/stanislavgerasko/Projects/geotraining/geoapi/server geoapi/proto/geoapi.proto`

PVC
```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
  name:  postgrespv
  labels:
    type: local
spec:
  capacity:
    storage: 3000Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/Volumes/Photos/pg"
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: postgrespvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3000Gi
```